package color;
import java_cup.runtime.Symbol;

%%
/* segunda parte: declaramos las directivas y los macros */

%class AnalizadorLexico
%public
%full
%unicode
%line
%column
%char
%cup

SaltoLinea = \r|\n|\r\n|\n\r
espacios = {SaltoLinea} | [  \t\f]
ValorEntero = 0|[1-9][0-9]*
ValorTexto = [a-zA-Z][a-zA-Z]*


/*Comentario = [//] [a-z][a-z]*[0-9] */

%%
/* OPERADORES Y SIGNOS */

"," {return new Symbol(sym.COMA, new token(yycolumn, yyline, yytext()));}
"(" {return new Symbol(sym.ABRIRPAR, new token(yycolumn, yyline, yytext()));}
")" {return new Symbol(sym.CERRARPAR, new token(yycolumn, yyline, yytext()));}

/* PALABRAS RESERVADAS */
"<inicio>" {return new Symbol(sym.INICIO, new token(yycolumn, yyline, yytext()));}
"</fin>" {return new Symbol(sym.FIN, new token(yycolumn, yyline, yytext()));}
"<cadena>" {return new Symbol(sym.CADENA, new token(yycolumn, yyline, yytext()));}
"<color>" {return new Symbol(sym.COLOR, new token(yycolumn, yyline, yytext()));}

/* EXPRESIONES */

{ValorTexto} {return new Symbol(sym.VALTEXT, new token(yycolumn, yyline, yytext()));}   
{ValorEntero} {return new Symbol(sym.VALINT, new token(yycolumn, yyline, yytext()));}
{SaltoLinea} {return new Symbol(sym.ENTER, new token(yycolumn, yyline, yytext()));}
{espacios} {/* ignorar */}
. {System.err.println("caracter invalido" + yytext() + "["+ yyline + ":"+ yycolumn + "]");}