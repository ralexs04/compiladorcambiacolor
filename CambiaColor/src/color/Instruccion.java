
package color;

/**
 *
 * @author ralexs
 */

public class Instruccion {
    //esta clase sirve para guardar los datos de cada una de las intrucciones
    String instruccion;//guarda el nombre de la instruccion:  cadena, color
    int[] numeros;//guarda los numeros correspondientes de cada instruccion
    String  cadena;//guarda los numeros correspondientes de cada instruccion

    public Instruccion() {
        
    }

    
    public Instruccion(String instruccion, int[] numeros) {
        this.instruccion = instruccion;
        this.numeros = numeros;
    }
    public Instruccion(String instruccion, String cadena) {
        this.instruccion = instruccion;
        this.cadena = cadena;
    }

    public String getInstruccion() {
        return instruccion;
    }

    public int[] getNumeros() {
        return numeros;
    }

    public String getCadena() {
        return cadena;
    }
    
}

