
package color;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JPanel;

public class Dibujo extends JPanel {
    //clase en la que se dibujan las figuras
    ArrayList<Instruccion> instrucciones;//istrucciones que se van a ejecutar

    public Dibujo(ArrayList<Instruccion> instrucciones) {      
        this.instrucciones=instrucciones;
    }

    public Dibujo() {
    }
    
    /*
     * clase que se encarga de comparar el nombre de la instruccion y dibujar la figura correspondiente
     */
    private void dibujarForma(Graphics g, Instruccion instruccion){
        switch(instruccion.getInstruccion()){
            case "cadena":
                Graphics2D g2d = (Graphics2D)g;
                int screenRes = Toolkit.getDefaultToolkit().getScreenResolution();
                int fontSize = (int)Math.round(30.0 * screenRes / 72.0);
                Font font = new Font("Arial", Font.PLAIN, fontSize);
                g2d.setFont(font);
                g2d.drawString(instruccion.getCadena(), 50, 50);
                break;

            case "color":
                g.setColor(new Color(instruccion.numeros[0], instruccion.numeros[1], instruccion.numeros[2]));
                break;
        }
    }  

    @Override
    public void paintComponent( Graphics g )
    {
        //metodo sobrecargado para que se pueda dibujar
        
        super.paintComponent(g);
        this.setBackground( Color.WHITE );
        for(int i=0; i<instrucciones.size(); i++) {
            dibujarForma(g,instrucciones.get(i));
        }
    }
}
