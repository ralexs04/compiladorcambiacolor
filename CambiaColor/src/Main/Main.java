package Main;

import color.AnalizadorLexico;
import color.AnalizadorSintactico;
import color.Dibujo;
import color.Instruccion;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ralexs
 */
public class Main extends javax.swing.JFrame {
    Color color;
    String colorrgb;
    public Main() {
        initComponents();
        setLocationRelativeTo(null);
        lblImg.setVisible(false);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ContenedorMenu = new javax.swing.JPanel();
        btnIniciar = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtTexto = new javax.swing.JTextArea();
        btnEnviar = new javax.swing.JButton();
        Salir = new javax.swing.JButton();
        contenedorTitulo = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        contenedorEditor = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        editorCodigo = new javax.swing.JEditorPane();
        lblImg = new javax.swing.JLabel();
        contenedorEstructura = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        contenedorResultado = new javax.swing.JPanel();
        lblTexto = new javax.swing.JLabel();
        contenedorErrores = new javax.swing.JPanel();
        lblError = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("contenedorPrincipal"); // NOI18N

        ContenedorMenu.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED)));

        btnIniciar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/boton-inicio_opt.png"))); // NOI18N
        btnIniciar.setText("Iniciar");
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Seleccione Color --", "Azul", "Cyan", "drak_gray", "amarillo", "verde" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        txtTexto.setColumns(20);
        txtTexto.setRows(2);
        jScrollPane3.setViewportView(txtTexto);

        btnEnviar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/icono_opt.png"))); // NOI18N
        btnEnviar.setText("Ejecutar");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        Salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/p5_opt.png"))); // NOI18N
        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ContenedorMenuLayout = new javax.swing.GroupLayout(ContenedorMenu);
        ContenedorMenu.setLayout(ContenedorMenuLayout);
        ContenedorMenuLayout.setHorizontalGroup(
            ContenedorMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ContenedorMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ContenedorMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Salir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, ContenedorMenuLayout.createSequentialGroup()
                        .addGroup(ContenedorMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnEnviar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnIniciar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        ContenedorMenuLayout.setVerticalGroup(
            ContenedorMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ContenedorMenuLayout.createSequentialGroup()
                .addComponent(btnIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Salir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblTitulo.setFont(new java.awt.Font("Andalus", 1, 24)); // NOI18N
        lblTitulo.setText("Compilador-Cambiar el color a una palabra ");

        javax.swing.GroupLayout contenedorTituloLayout = new javax.swing.GroupLayout(contenedorTitulo);
        contenedorTitulo.setLayout(contenedorTituloLayout);
        contenedorTituloLayout.setHorizontalGroup(
            contenedorTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, contenedorTituloLayout.createSequentialGroup()
                .addContainerGap(165, Short.MAX_VALUE)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(107, 107, 107))
        );
        contenedorTituloLayout.setVerticalGroup(
            contenedorTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorTituloLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        contenedorEditor.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Terminal", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jScrollPane1.setViewportView(editorCodigo);

        lblImg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cargando.gif"))); // NOI18N

        javax.swing.GroupLayout contenedorEditorLayout = new javax.swing.GroupLayout(contenedorEditor);
        contenedorEditor.setLayout(contenedorEditorLayout);
        contenedorEditorLayout.setHorizontalGroup(
            contenedorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorEditorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(contenedorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                    .addGroup(contenedorEditorLayout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(lblImg)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        contenedorEditorLayout.setVerticalGroup(
            contenedorEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorEditorLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblImg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        contenedorEstructura.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Estructura Programa", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Modern No. 20", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText("1.- Inicio\n2.- Color\n3.- Cadena\n4.- Fin\n\n\nEjemplo: \n\n<inicio>\n\n<color>(5,4,2)<color>\n\n<cadena>(HolaCompilador)<cadena>\n\n</fin>\n");
        jScrollPane2.setViewportView(jTextArea1);

        javax.swing.GroupLayout contenedorEstructuraLayout = new javax.swing.GroupLayout(contenedorEstructura);
        contenedorEstructura.setLayout(contenedorEstructuraLayout);
        contenedorEstructuraLayout.setHorizontalGroup(
            contenedorEstructuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        contenedorEstructuraLayout.setVerticalGroup(
            contenedorEstructuraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
        );

        contenedorResultado.setBackground(new java.awt.Color(0, 0, 0));
        contenedorResultado.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 255, 204)), javax.swing.BorderFactory.createTitledBorder(null, "Salida", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14), new java.awt.Color(204, 0, 204)))); // NOI18N

        lblTexto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        javax.swing.GroupLayout contenedorResultadoLayout = new javax.swing.GroupLayout(contenedorResultado);
        contenedorResultado.setLayout(contenedorResultadoLayout);
        contenedorResultadoLayout.setHorizontalGroup(
            contenedorResultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorResultadoLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(lblTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 730, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        contenedorResultadoLayout.setVerticalGroup(
            contenedorResultadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contenedorResultadoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        contenedorErrores.setBackground(new java.awt.Color(0, 0, 0));
        contenedorErrores.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Errores", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12), new java.awt.Color(255, 0, 204))); // NOI18N
        contenedorErrores.setForeground(new java.awt.Color(0, 255, 0));
        contenedorErrores.setLayout(new java.awt.BorderLayout());

        lblError.setForeground(new java.awt.Color(0, 255, 0));
        contenedorErrores.add(lblError, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(contenedorTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 61, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(ContenedorMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(contenedorResultado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(contenedorEditor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(contenedorEstructura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(contenedorErrores, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(contenedorTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(contenedorEditor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(contenedorEstructura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(contenedorResultado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(contenedorErrores, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ContenedorMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        lblError.setText("");
        lblTexto.setText("");
        txtTexto.setText("");
        txtTexto.setBackground(Color.WHITE);
        lblImg.setVisible(true);
        
        
        String datos = editorCodigo.getText();
        //if (datos.isEmpty()) {
            editorCodigo.setText("<inicio> \n" + "<color><color>" + "\n<cadena>()<cadena>" + "\n </fin>");
        //}
    }//GEN-LAST:event_btnIniciarActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
     lblError.setText("");
     lblTexto.setText("");
     txtTexto.setText("");
     lblImg.setVisible(false);
        ejecutar();
    }//GEN-LAST:event_btnEnviarActionPerformed

    
public void ejecutar(){
    try{       
           
            escribir("programa.txt");//se guarda en un archivo externo lo que se escribio
            /*se le pasa al analizador lexico lo que se escribio en el programa.txt y se envia el lblError  para 
            presentar  por pantalla los errores correspondientes*/
            AnalizadorLexico lex = new AnalizadorLexico(new FileReader("programa.txt"), lblError);
            //Se le pasa al analizador sitactico lo que genero el analizador lexico para la
            //debida corresccion sintactica
            AnalizadorSintactico parser = new AnalizadorSintactico(lex, lblError);
            try{
                parser.parse(); //se encarga de verificar todo y la ejecucion del respestivo codigo
                if(lblError.getText().isEmpty())
                    //obtenemos cada una de las instrucciones establecidas
                    recibirInstrucciones(parser.getInstrucciones()); 
            }
            catch (java.lang.Exception el)
            {
                 lblError.setText( "error encontrado: "+el.getMessage());
            }
        } 
        catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ex " + ex);
        }
}
   
    public void escribir(String direccion){
        //metodo que guarda lo que esta escrito en un archivo de texto
	try{	
            FileWriter writer = new FileWriter(direccion);
            PrintWriter print = new PrintWriter(writer);
            //obtenemos el texto del JTexArea  para agregarlo al archivo que vamos a escribir
            print.print(editorCodigo.getText());
            writer.close(); //esribimos el archivo correspondiente
            
	}
            catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    
    public void recibirInstrucciones(ArrayList<Instruccion>  instrucciones){
         
        for(int i=0; i<instrucciones.size(); i++) {
            escribirPalabra(instrucciones.get(i));
        }
    }
    private void escribirPalabra( Instruccion instruccion){
        switch(instruccion.getInstruccion()){
            case "cadena":
                lblTexto.setText(instruccion.getCadena());
                break;
            case "color":
                Color color = new  Color(instruccion.getNumeros()[0], instruccion.getNumeros()[1], instruccion.getNumeros()[2]);
                lblTexto.setForeground(color);
                 break;
        }
    }  
    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        this.colores();
        txtTexto.setBackground(color);
        txtTexto.setText("(" + colorrgb + ")");
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
       System.exit(1);
    }//GEN-LAST:event_SalirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);

            }
        });
    }

    private void colores() {

        if (this.jComboBox1.getSelectedItem() == "Azul") {
            color = Color.BLUE;
            colorrgb = "0,0,255";
        }
        if (this.jComboBox1.getSelectedItem() == "Cyan") {
            color = Color.CYAN;
            colorrgb = "0,255,255";
        }
        if (this.jComboBox1.getSelectedItem() == "drak_gray") {
            color = Color.DARK_GRAY;
            colorrgb = "64,64,64";
        }
        if (this.jComboBox1.getSelectedItem() == "amarillo") {
            color = Color.YELLOW;
            colorrgb = "255,255,0";
        }
        if (this.jComboBox1.getSelectedItem() == "verde") {
            color = Color.green;
            colorrgb = "0,255,0";
        }

    }

    
  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ContenedorMenu;
    private javax.swing.JButton Salir;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnIniciar;
    private javax.swing.JPanel contenedorEditor;
    private javax.swing.JPanel contenedorErrores;
    private javax.swing.JPanel contenedorEstructura;
    private javax.swing.JPanel contenedorResultado;
    private javax.swing.JPanel contenedorTitulo;
    private javax.swing.JEditorPane editorCodigo;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lblError;
    private javax.swing.JLabel lblImg;
    private javax.swing.JLabel lblTexto;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextArea txtTexto;
    // End of variables declaration//GEN-END:variables

}
