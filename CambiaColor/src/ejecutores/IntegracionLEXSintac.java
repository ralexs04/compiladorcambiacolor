package ejecutores;
    
import color.AnalizadorLexico;
import color.AnalizadorSintactico;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author ralexs
 */
public class IntegracionLEXSintac {
    
    public void ejecutar(){
    
        try {
            
         
                    
            BufferedReader bf = new BufferedReader(new FileReader("src/color/Archivo.txt"));
            AnalizadorLexico lex = new AnalizadorLexico(bf, null);
            AnalizadorSintactico parser = new AnalizadorSintactico(lex);
            parser.parse();
            } catch (Exception e) {
                System.out.println("error en "+ e.getMessage());
                
        }
    }
    public static void main(String[] args) {
        new IntegracionLEXSintac().ejecutar();
    }
         
}
